(function($, CUI){

    CUI.rte.plugins.touchUiDemo = CUI.rte.plugins.touchUiDemo || {};

    CUI.rte.plugins.touchUiDemo.HighlightPlugin = new Class({

        toString: "touchUiDemo.HighlightPlugin",

        pluginId: "touchUi-highlight",

        extend: CUI.rte.plugins.Plugin,

        AVAILABLE_FEATURES: {
            HIGHLIGHT: "highlight"
        },

        getFeatures: function() {
            return [ this.AVAILABLE_FEATURES.HIGHLIGHT ];
        },

        initializeUI: function(tbGenerator, options) {
            if (this.isFeatureEnabled(this.AVAILABLE_FEATURES.HIGHLIGHT)) {
                tbGenerator.registerIcon(
                    this.pluginId + "#" + this.AVAILABLE_FEATURES.HIGHLIGHT,
                    "efficient"
                );

                tbGenerator.addElement(
                    this.pluginId,
                    0,
                    tbGenerator.createElement(
                        this.AVAILABLE_FEATURES.HIGHLIGHT,
                        this,
                        true
                    ),
                    0
                );
            }
        },

        execute: function(pluginCommand, value, envOptions) {
            let $content = $(envOptions.editContext.root);
            if ($content.data("mark-applied")) {
                $content.unmark();
                $content.data("mark-applied", false)
            } else {
                $content.mark(["get", "gets", "getting"], { accuracy: "exactly" });
                $content.data("mark-applied", true)
            }
        },
    });

    CUI.rte.plugins.PluginRegistry.register("touchUi-highlight", CUI.rte.plugins.touchUiDemo.HighlightPlugin);

}(jQuery, window.CUI));
