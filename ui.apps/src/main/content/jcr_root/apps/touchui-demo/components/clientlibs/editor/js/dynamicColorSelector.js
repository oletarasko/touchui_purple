
(function ($, $document) {
    "use strict";

    // in this example we have duplicated options in dialog definition and in JS.
    // using DataSource is one of the options to overcome this issue.
    $(document).ready(function() {
        var mainColorSelector = $document.find(".js-main-color").get(0);
        var secondaryColorSelector = $document.find(".js-secondary-color").get(0);

        var selectedMainValue = mainColorSelector.selectedItem.value;
        var selectedMainTextContent = mainColorSelector.selectedItem.textContent;

        var secondaryItems = secondaryColorSelector.items;


        secondaryColorSelector.addEventListener('coral-select:showitems', function(event) {

            event.preventDefault();
            if (!mainColorSelector.selectedItem) {
                return;
            }

            var currentlySelectedItem = secondaryColorSelector.selectedItem;
            secondaryColorSelector.items.clear();

            var secondaryColorsDraft = [
                {
                    value: 'light',
                    textContent: "Light"
                },
                {
                    value: 'dark',
                    textContent: "Dark"
                },
            ];

            var selectedMainValue = mainColorSelector.selectedItem.value;
            var selectedMainTextContent = mainColorSelector.selectedItem.textContent;
            secondaryColorsDraft.forEach(function(draftColorOption) {
                secondaryColorSelector.items.add({
                    value: draftColorOption.value + '-' + selectedMainValue,
                    content: {
                        textContent: draftColorOption.textContent + ' ' + selectedMainTextContent
                    }
                });
            });
        });
    });

    $document.on("change", ".js-main-color", function() {

        var secondaryColorSelector = $document.find(".js-secondary-color").get(0);

        secondaryColorSelector.items.clear();
    });
})($, $(document));