<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page session="false"%><%
%><%@page import="java.util.Collection,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.cq.wcm.launches.utils.LaunchUtils,
                  com.day.cq.wcm.msm.api.LiveRelationship,
                  com.day.cq.wcm.msm.api.LiveRelationshipManager,
                  com.day.cq.wcm.msm.api.LiveStatus,
                  com.day.cq.commons.jcr.JcrConstants,
                  java.util.Calendar,
                  java.util.Date,
                  com.day.cq.wcm.api.PageManager,
                  javax.jcr.RangeIterator"%><%

    Config cfg = cmp.getConfig();
    String path = cmp.getExpressionHelper().getString(cfg.get("path", ""));
    String href = cfg.get("href", "/mnt/overlay/wcm/core/content/sites/properties.html");

    if ("".equals(path)) {
        return;
    }

    LiveRelationshipManager relationshipManager = sling.getService(LiveRelationshipManager.class);
    Resource target = slingRequest.getResourceResolver().getResource(path);

%><div class="coral-Form-fieldwrapper"><%
    RangeIterator rangeIterator = relationshipManager.getLiveRelationships(target, null, null);
    while(rangeIterator.hasNext()) {
        LiveRelationship relationship = (LiveRelationship) rangeIterator.next();
        String lcPath = relationship.getTargetPath();
        LiveStatus status = relationship.getStatus();

        if (status.isTargetExisting() && !LaunchUtils.isLaunchResourcePath(lcPath)) {
            String lcHref = href + (!href.contains("?") ? "?" : "&") +  "item=" + lcPath;
            lcHref = lcHref + "&backHref=/sites.html" + lcPath;
            String text = lcPath.endsWith("/" + JcrConstants.JCR_CONTENT) ?
                lcPath.substring(0, lcPath.lastIndexOf("/" + JcrConstants.JCR_CONTENT)) :
                lcPath;

            Date lastRollout = status.getLastRolledOut();
            Calendar lastPageModification = resourceResolver.adaptTo(PageManager.class).getPage(target.getPath()).getLastModified();

            // We are checking only top level Live Copy page and not nested.
            boolean isUpToDate = true;
            if (lastPageModification != null) {
                isUpToDate = lastPageModification.equals(lastRollout) || lastPageModification.getTime().before(lastRollout);
            }

            %><div style="margin-bottom:4px;">
                <coral-checkbox class="js-rollout-checkbox" value="<%= xssAPI.getValidHref(lcPath) %>">
                    <a href="<%= xssAPI.getValidHref(lcHref) %>" style="color:<%= isUpToDate ? "green" : "red" %>">
                        <%= xssAPI.encodeForHTML(lcPath) %>
                    </a>
                </coral-checkbox>
            </div><%
        }
    }
%>

    <button class="js-rollout-trigger" is="coral-button" variant="minimal" icon="publish" iconsize="M">
        Activate selected
    </button>
</div>