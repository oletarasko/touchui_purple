$(window).adaptTo("foundation-registry").register("foundation.adapters", {
    type: "touchui-form-helper",
    selector: "form.foundation-form",
    adapter: function(form) {
        var $form = $(form);

        return {
            getByName: function(inputName) {
                return $form.find("[name='" + inputName + "']");
            },
            highlight: function(inputName) {
                $form.find("[name='" + inputName + "']").attr("invalid", "");
            }
        };
    }
});
