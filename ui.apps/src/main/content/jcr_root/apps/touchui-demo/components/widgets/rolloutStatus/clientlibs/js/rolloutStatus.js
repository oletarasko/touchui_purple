
(function ($, $document, $window) {
    "use strict";

    $document.ready(function() {

        $document.on("click", ".js-rollout-trigger", function() {
            var checked = $(".js-rollout-checkbox").filter(function() {
                return this.checked;
            });

            var values = $.map(checked, function(el) {
                return el.value;
            })

            var foundationUi = $window.adaptTo("foundation-ui");
            var ticker = foundationUi.waitTicker("Wait for rollout to complete");

            //time to send values to our servlet
            //and check result of rollout
            console.log(values);

            setTimeout(function(){
                ticker.clear();
                var successful = true;
                var title = "Rollout finished";
                var message = "Selected livecopies were successfully updated.";
                foundationUi.notify(title, message, "info");

                if (successful) {
                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                }
            }, 4000);
        });
    });


})($, $(document), $(window));