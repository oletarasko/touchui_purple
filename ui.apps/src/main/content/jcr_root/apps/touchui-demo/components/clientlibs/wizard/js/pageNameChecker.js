$(window).adaptTo("foundation-registry").register("foundation.form.submit", {
    selector: ".cq-siteadmin-admin-createpage",
    handler: function(form) {

        //do some BE validation of page name+title
        let formUtil = $(form).adaptTo("touchui-form-helper");
        let pageNameInput = formUtil.getByName("pageName");

        let shouldSkipValidation = pageNameInput.data("skipNameValidation");

        let containsStopWord = !pageNameInput.val() || pageNameInput.val().toLowerCase().indexOf("stopword") > -1;
        if (!shouldSkipValidation && containsStopWord) {
            $(window).adaptTo("foundation-ui")
                .prompt("Page name contains stop word!",
                    "Page name you provided contains stop word. This may affect SEO ranking! Please, consider canceling page creation and adjusting page name",
                    "notice",
                    [
                        {
                            id: "cancel",
                            text: "Cancel page creation",
                            primary: true,
                            handler: (action) => {
                                formUtil.highlight("pageName");
                            }
                        },
                        {
                            id: "create",
                            text: "I know what I do. Continue.",
                            warning: true,
                            handler: (action) => {

                                // we can get into the validation loop if we do not specify that it's 2nd iteration of submit.
                                pageNameInput.data("skipNameValidation", true);
                                $(form).submit();
                            }
                        }
                    ]);
        }

        return {
            preResult: !shouldSkipValidation && containsStopWord ? $.Deferred().reject() : $.Deferred().resolve(), //tried ES6 Promise, but it always was treated as resolved
        }
    }
});
