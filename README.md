Demo code for the TouchUI purple badge.

Table of content:

* custom widgets:
* * Automatically construct and use path to DAM with regard to the current page (pathbrowser extension)- /apps/touchui-demo/components/widgets/damPathBrowser
* * Show if livecopy of current page is up-to-date + ability to activate then (no backend implementation, copy+extension of wcm/msm/components/coral/touch-ui/propertiesdialog/blueprint/livecopies) - /apps/touchui-demo/components/widgets/rolloutStatus
* custom field validation - /apps/touchui-demo/components/clientlibs/editor/js/editDialog.js
* custom project tile for showing number of Not Activated Pages per market/locale - /etc/projects/dashboard/gadgets/activationStatistics and /apps/touchui-demo/components/activationStatistics
* widget (IP address field) which is shown if user has jcr:all privilege for current page - /apps/touchui-demo/components/customPage/_cq_dialog/.content.xml
* JS for 2 drop-downs where 1 depends on selection of another - /apps/touchui-demo/components/clientlibs/editor/js/dynamicColorSelector.js
* show in Sites console in column view if page is activated - /apps/cq/gui/components/coral/admin/page/columnitem
* show warning window to the editor on page creation in case page name contains seo stop word - /apps/touchui-demo/components/clientlibs/wizard/js/pageNameChecker.js
* example of adapter pattern - /apps/touchui-demo/components/clientlibs/formUtil/js/formUtil.js
* RTE plugin for highlighting "stop words" in SEO - /apps/touchui-demo/components/clientlibs/rteHighlight/js/highlightRte.js
* drop target, changing actions set, refresh mode example - /apps/touchui-demo/components/testImageComponent
