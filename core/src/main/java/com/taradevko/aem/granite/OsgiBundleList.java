package com.taradevko.aem.granite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;

@Model(adaptables = SlingHttpServletRequest.class, resourceType = "granite/ui/components/coral/foundation/anchorbutton")
@Exporter(name = "jackson", extensions = "json", selector = "osgiBundles")
public class OsgiBundleList {

	@Self
	private SlingHttpServletRequest request;

	private List<Resource> bundleResourceList;

	@PostConstruct
	protected void init() {
		Resource resource = request.getResource();
		Resource dataSource = resource.getChild("datasource");
		ResourceResolver resolver = resource.getResourceResolver();

		String itemResourceType = null;
		if (dataSource != null) {
			ValueMap dsProperties = dataSource.getValueMap();
			itemResourceType = dsProperties.get("itemResourceType", String.class);
		}

		BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		bundleResourceList = new ArrayList<>();
		for (Bundle bundle : bundleContext.getBundles()) {
			Map<String, Object> props = new HashMap<>();
			props.put("bName", bundle.getSymbolicName());
			props.put("bVersion", bundle.getVersion().toString());
			props.put("bStatus", getStatusString(bundle.getState()));
			bundleResourceList.add(new ValueMapResource(resolver, "", itemResourceType, new ValueMapDecorator(props)));
		}

		DataSource ds = new SimpleDataSource(bundleResourceList.iterator());
		request.setAttribute(DataSource.class.getName(), ds);
	}

	private String getStatusString(final int state) {
		switch (state) {
			case Bundle.ACTIVE:
				return "ACTIVE";
			case Bundle.INSTALLED:
				return "INSTALLED";
			case Bundle.RESOLVED:
				return "RESOLVED";
			case Bundle.STARTING:
				return "STARTING";
			default:
				return "OTHER";
		}
	}

	public List<Resource> getBundleResourceList() {
		return bundleResourceList;
	}
}
