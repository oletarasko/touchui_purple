package com.taradevko.aem.granite;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

public class DamPathHelper {

    private static final String ROOT_PATH = "/content/dam/we-retail";
    private static final String BLUEPRINT = "language-masters";

    public static String getRootPath(final SlingHttpServletRequest request) {
        String resourcePath = request.getRequestPathInfo().getSuffix();

        if (resourcePath == null) {
            return ROOT_PATH;
        }

        boolean isBlueprint = resourcePath.contains(BLUEPRINT);

        Matcher matcher = getPattern(isBlueprint).matcher(resourcePath);
        if (matcher.find()) {
            String locale = getLocale(matcher, isBlueprint);
            String pageRelPath = matcher.group(3);

            String localizedDamPath = String.format("%s/%s/%s", ROOT_PATH, locale, pageRelPath);

            ResourceResolver resourceResolver = request.getResourceResolver();
            if (resourceResolver.getResource(localizedDamPath) != null) {
                return localizedDamPath;
            }

            String baseDamPath = String.format("%s/base/%s", ROOT_PATH, pageRelPath);
            if (resourceResolver.getResource(baseDamPath) != null) {
                return baseDamPath;
            }
        }

        return ROOT_PATH;
    }

    private static String getLocale(final Matcher matcher, final boolean isBlueptint) {
        if (isBlueptint) {
            return matcher.group(1);
        } else {
            return matcher.group(2);
        }
    }

    private static Pattern getPattern(final boolean isBlueprint) {
        if (isBlueprint) {
            return Pattern.compile("^.*/(\\w{2})(/language-masters)?/(.*)/jcr:content.*$");
        } else {
            return Pattern.compile("^.*/(\\w{2})/(\\w{2})/(.*)/jcr:content.*$");
        }
    }
}
